# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "s3-job-offer-bucket-richel-deydier-urdes-bonin" {
  bucket = var.s3_user_bucket_name
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }

  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "s3_job_offer_bucket" {
  bucket = var.s3_user_bucket_name
  acl    = "private"
  key    = "raw/"
  source = "job_offers/raw"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
